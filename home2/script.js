let nummers = [1, 2, 3, 4];

const midNumberDisplay = document.getElementById('result');

//сортировка массива
function sortNumAll() {
	function sortNum(a, b) {
		if (a > b) {
			return 1;
		}
		if (a < b) {
			return -1;
		}
	} // sortNum
	return nummers.sort(sortNum);
} // sortNumAll

//вычисление медианы
function midNum() {
	let sortedNum = sortNumAll();
    let mid = Math.floor(sortedNum.length/2);
    if (sortedNum.length % 2) {
        return sortedNum[mid];
    } else {
        return (sortedNum[mid-1] + sortedNum[mid]) / 2 ;
    }

} //midNum

//вывод медианы
console.log(midNum());